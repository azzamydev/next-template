import clsx from "clsx";
import Link from "next/link";
import { useEffect, useState } from "react";
import { ChevronUp } from "react-bootstrap-icons";
import { Collapse } from "react-collapse";
import { useSelector } from "react-redux";
import "react-tooltip/dist/react-tooltip.css";
import { Tooltip } from "react-tooltip";

const Menus = ({ children }) => {
    return <ul className="flex flex-col gap-y-1 my-5">{children}</ul>;
};

const Item = ({ path, isActive = false, icon, label }) => {
    const { app } = useSelector((state) => state);

    return (
        <Link href={path}>
            <li
                data-tooltip-id={label}
                data-tooltip-content={label}
                data-tooltip-place="right"
                className={clsx(
                    `cursor-pointer px-3 py-2 rounded-lg flex gap-x-3 items-center transition-all duration-200 ${
                        isActive
                            ? "bg-primary-500 text-white"
                            : "hover:bg-primary-500/10"
                    } ${app.sidebar ? "justify-start" : "justify-center"}`
                )}>
                {icon}
                <p
                    className={`${
                        app.sidebar ? "inline-block" : "hidden"
                    } whitespace-nowrap text-sm`}>
                    {label}
                </p>
                {!app.sidebar ? <Tooltip id={label} /> : null}
            </li>
        </Link>
    );
};

const Group = ({ children, groupLabel = "Group" }) => {
    const { app } = useSelector((state) => state);
    const [isShow, setIsShow] = useState(true);

    useEffect(() => {
        if (!app.sidebar) {
            setIsShow(true);
        }
    }, [app.sidebar]);
    return (
        <div className="border-t mt-2 pb-3">
            <div
                className={`px-3 justify-between items-center py-3 text-sm text-gray-600 ${
                    app.sidebar ? "flex" : "hidden"
                }`}>
                <p className="capitalize">{groupLabel}</p>
                <ChevronUp
                    onClick={() => setIsShow((e) => !e)}
                    className={`cursor-pointer transition-all duration-300 ${
                        isShow ? "rotate-0" : "rotate-180"
                    }`}
                />
            </div>
            <Collapse isOpened={isShow}>{children}</Collapse>
        </div>
    );
};

Menus.Item = Item;
Menus.Group = Group;

export default Menus;
