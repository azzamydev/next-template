import clsx from "clsx";
import React, { FC, ReactNode, useEffect, useState } from "react";

interface Props {
    type?: "button" | "submit" | "reset";
    children?: ReactNode;
    className?: string;
    color?: "primary" | "secondary" | "warning" | "danger" | "success";
    onClick?: () => void;
}

const Button: FC<Props> = ({
    onClick,
    type = "button",
    children,
    className = "",
    color = "primary",
}) => {
    const [background, setBackground] = useState<string>();

    useEffect(() => {
        switch (color) {
            case "warning":
                setBackground("bg-warning-500 text-white hover:bg-warning-600");
                break;
            case "success":
                setBackground("bg-success-500 text-white hover:bg-success-600");
                break;
            case "danger":
                setBackground("bg-danger-500 text-white hover:bg-danger-600");
                break;
            case "secondary":
                setBackground(
                    "bg-secondary-500 text-white hover:bg-secondary-600"
                );
                break;

            default:
                setBackground("bg-primary-500 text-white hover:bg-primary-600");
                break;
        }
    }, [color]);

    return (
        <button
            onClick={() => onclick}
            type={type}
            className={clsx(
                className,
                `${background} flex gap-x-2 items-center px-5 py-2 rounded-full shadow min-w-[200px] justify-center duration-200`
            )}>
            {children}
        </button>
    );
};

export default Button;
