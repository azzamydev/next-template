import React from "react";
import { BrowserEdge } from "react-bootstrap-icons";
import { useSelector } from "react-redux";

const Brand = () => {
    const { app } = useSelector((state) => state);
    return (
        <div className="flex items-center justify-center gap-x-3 overflow-hidden py-2 text-primary-600">
            <BrowserEdge size={30} />
            <h1
                className={`${
                    app.sidebar ? "inline-block" : "hidden"
                } text-2xl font-bold`}>
                Brand
            </h1>
        </div>
    );
};

export default Brand;
