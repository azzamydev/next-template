import {
    Columns,
    GearWideConnected,
    House,
    Person,
} from "react-bootstrap-icons";
import { useSelector } from "react-redux";
import Brand from "../widget/Brand";
import Menus from "../widget/Menus";

const menus = [
    {
        label: "Dashboard",
        path: "/",
        isActive: true,
        icon: <House size={20} />,
    },
    {
        label: "Profile",
        path: "/profile",
        icon: <Person size={20} />,
    },
];
const groupMenus = [
    {
        label: "Widgets",
        path: "/widgets",
        icon: <GearWideConnected size={20} />,
    },
    {
        label: "Layouts",
        path: "/layouts",
        icon: <Columns size={20} />,
    },
];

const Sidebar = () => {
    const { app } = useSelector((state) => state);
    const path = window.location.pathname;

    return (
        <aside
            className={`fixed border-r md:relative inset-y-0 md:h-full z-30 md:z-0 shadow-xl  bg-white p-3 transition-all duration-300 ${
                app.sidebar
                    ? "translate-x-0 md:translate-x-0 w-[300px] "
                    : "-translate-x-full md:translate-x-0 w-[80px]"
            }`}>
            <div className="h-full w-full  ">
                <Brand />
                <Menus>
                    {menus.map((item, index) => (
                        <Menus.Item
                            key={index}
                            path={item.path}
                            isActive={path == item.path}
                            icon={item.icon}
                            label={item.label}
                        />
                    ))}
                    <Menus.Group>
                        {groupMenus.map((item, index) => (
                            <Menus.Item
                                key={index}
                                path={item.path}
                                isActive={path == item.path}
                                icon={item.icon}
                                label={item.label}
                            />
                        ))}
                    </Menus.Group>
                </Menus>
            </div>
        </aside>
    );
};

export default Sidebar;
