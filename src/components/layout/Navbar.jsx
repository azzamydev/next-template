import { toggle } from "@/redux/slices/AppSlice";
import Image from "next/image";
import { useState } from "react";
import { ChevronDown, DoorOpenFill, List } from "react-bootstrap-icons";
import { useDispatch } from "react-redux";

const NavMenu = () => {
    const [show, setShow] = useState(false);
    return (
        <div className="flex items-center gap-x-5 relative">
            <div
                onClick={() => setShow(false)}
                className={`${
                    show ? "inline-block" : "hidden"
                } fixed inset-0 bg-transparent`}></div>
            <div
                className={`${
                    show ? "opacity-100" : "opacity-0"
                } transition-opacity duration-200 absolute top-10 right-0 md:right-10 shadow-md rounded-lg border bg-white overflow-hidden min-w-[250px] max-w-[300px]`}>
                <ul className="text-sm text-gray-600">
                    <li className=" cursor-pointer px-5 py-2 hover:bg-primary-50 duration-300">
                        Profile
                    </li>
                    <li className="border-t cursor-pointer px-5 py-2 hover:bg-primary-50 duration-300">
                        Lorem, ipsum dolor.
                    </li>
                    <li className="border-t cursor-pointer px-5 py-2 hover:text-red-500 duration-300">
                        <span className="flex items-center gap-x-1">
                            <DoorOpenFill />
                            Logout
                        </span>
                    </li>
                </ul>
            </div>
            <div
                onClick={() => setShow((e) => !e)}
                className="flex items-center gap-x-3 text-sm text-gray-600 hover:cursor-pointer">
                <p>Angga Baskara</p>
                <ChevronDown className={`${show ? 'rotate-180' : 'rotate-0'} duration-200`} />
            </div>
            <Image src={"/avatar.png"} width={40} height={40} alt="avatar" className="hidden md:inline-block" />
        </div>
    );
};
const Navbar = () => {
    const dispatch = useDispatch();
    const path = window.location.pathname.replace("/", "");

    return (
        <nav className="flex justify-between shadow px-3 md:px-10 py-3 bg-white">
            <div className="flex items-center gap-x-5">
                <List
                    size={25}
                    className={"hover:cursor-pointer text-primary-500"}
                    onClick={() => dispatch(toggle())}
                />
                {/* Breadcumb */}
                <ul className="flex items-center text-sm gap-x-1 text-gray-600">
                    <li className="capitalize">{path}</li>
                </ul>
            </div>
            <NavMenu />
        </nav>
    );
};

export default Navbar;
