import Head from 'next/head'
import React from 'react'

const Container = ({children, title}) => {
  return (
    <>
    <Head>
      <title>{title}</title>
    </Head>
    <div className='px-4 md:px-10 py-5'>
        {children}
    </div>
    </>
  )
}

export default Container