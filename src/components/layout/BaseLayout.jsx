import { closeSidebar } from "@/redux/slices/AppSlice";
import clsx from "clsx";
import { useDispatch, useSelector } from "react-redux";
import Navbar from "./Navbar";
import Sidebar from "./Sidebar";

const BaseLayout = ({ children, className }) => {
    const { app } = useSelector((state) => state);
    const dispatch = useDispatch();
    return (
        <div
            className={clsx(
                className,
                `h-screen bg-slate-100 overflow-hidden flex relative`
            )}>
            <div
                className={`inset-0 absolute md:hidden bg-black/30 transition-all duration-300 ${
                    app.sidebar ? "opacity-100 z-20" : "opacity-0 -z-10"
                }`}
                onClick={() => dispatch(closeSidebar())}
            />
            <Sidebar />
            <div className="w-full flex flex-col grow">
                <Navbar />
                {children}
            </div>
        </div>
    );
};

export default BaseLayout;
