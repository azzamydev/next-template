import BaseLayout from "@/components/layout/BaseLayout";
import { persistor, wrapper } from "@/redux/store";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import "@/styles/globals.css";

export default function App({ Component, ...rest }) {
    const { store, props } = wrapper.useWrappedStore(rest);
    return (
        <Provider store={store}>
            <PersistGate
                loading={null}
                persistor={persistor}
            >
                <BaseLayout>
                    <Component {...props.pageProps} />
                </BaseLayout>
            </PersistGate>
        </Provider>
    );
}
