import Container from '@/components/layout/Container'
import Button from '@/components/widget/Input/Button'
import React from 'react'

const Widgets = () => {
  return (
    <Container title={'Widgets'}>
        <div className='border-b py-5'>
            <h1 className='text-2xl font-medium'>Button</h1>
            <div className='py-5 flex gap-x-5 flex-wrap items-center'>
                <Button color='primary'>
                  Button
                </Button>
                <Button color='secondary'>
                  Button
                </Button>
                <Button color='success'>
                  Button
                </Button>
                <Button color='warning'>
                  Button
                </Button>
                <Button color='danger'>
                  Button
                </Button>
            </div>
        </div>
        <div className='border-b py-5'>
            <h1 className='text-2xl font-medium'>Textinput</h1>
            <div className='py-5 flex gap-x-5 flex-wrap items-center'>
               
            </div>
        </div>
    </Container>
  )
}

export default Widgets