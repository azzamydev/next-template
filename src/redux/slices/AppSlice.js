import { createSlice } from "@reduxjs/toolkit";

const AppSlice = createSlice({
    name: "app",
    initialState: {
        sidebar: true,
    },
    reducers: {
        toggle: (state) => {
            state.sidebar = !state.sidebar;
        },
        openSidebar: (state) => {
            state.sidebar = true;
        },
        closeSidebar: (state) => {
            state.sidebar = false;
        },
    },
});

export const { openSidebar, closeSidebar, toggle} = AppSlice.actions;
export default AppSlice.reducer;
