import { combineReducers, configureStore } from "@reduxjs/toolkit";
import storage from "redux-persist/lib/storage";
import { persistReducer, persistStore } from "redux-persist";
import { encryptTransform } from "redux-persist-transform-encrypt";
import AppSlice from "./slices/AppSlice";
import { createWrapper } from "next-redux-wrapper";

const rootReducer = combineReducers({
    app: AppSlice,
});

const persistConfig = {
    key: "root",
    storage,
    transforms: [
        // For Encrypted store
        encryptTransform({
            secretKey: "next",
            onError: function (error) {
                console.log(error);
            },
        }),
    ],
};

// create persis reducer and combine with root reducer (/src/features/rootReducer.js)
const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
    reducer: persistedReducer,
    // must include default middleware
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: false,
        }),
    devTools: true,
});

export const persistor = persistStore(store);
export const wrapper = createWrapper(() => store);
